import grpc
from grpcserver.proto import main_pb2, main_pb2_grpc
from typing import Dict

from grpcserver.models.subject import Subject
from grpcserver.models.category import Category


# Inherit from main_pb2_grpc.SubjectServiceServicer
# SubjectService is the server-side artifact
class SubjectService(main_pb2_grpc.SubjectServiceServicer):
	def CreateSubject(self, request, context):
		"""
		Create a subject.
		gRPC calls this method when clients call the CreateSubject rpc (method).
		Arguments:
			request (GetUserRequest): The incoming request.
			context: The gRPC connection context.
		Returns:
			subject (Subject): A subject.
		"""
		subject = Subject()

		name = request.name
		level = request.level
		country = request.country
		description = request.description
		category = Category.find_by_name(name=request.category)
		if not category:
			context.set_code(grpc.StatusCode.NOT_FOUND)
			context.set_details(f"Category with name '{request.category}' is not available")
			return main_pb2.SubjectResponse()

		if not name or not level or not country or not category:
			context.set_code(grpc.StatusCode.UNIMPLEMENTED)
			context.set_details("All fields including {name, level, country, category} are required")
			return main_pb2.SubjectResponse()

		subject_exist = Subject.find_by_name(name=name)
		if subject_exist is not None:
			context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
			context.set_details(f"Subject with name '{request.name}' already exists")
			return main_pb2.SubjectResponse()
		new_subject = subject.create(name=name, description=description, category=category.id, country=country, level=level)
		subject_result = main_pb2.Subject(
			uuid=new_subject.uuid,
			name=new_subject.name,
			level=new_subject.level,
			country=new_subject.country,
			category=request.category,
			description=new_subject.description
		)
		return main_pb2.SubjectResponse(subject=subject_result)

	def GetSubject(self, request, context):
		"""
		Gets a subject.
		gRPC calls this method when clients call the GetSubject rpc (method).
		Arguments:
			request (GetUserRequest): The incoming request.
			context: The gRPC connection context.
		Returns:
				subject (Subject): A subject.
		"""
		find_by: Dict[str, str] = {}
		if request.name:
			find_by['name'] = request.name
		if request.level:
			find_by['level'] = request.level
		if request.country:
			find_by['country'] = request.country
		if request.category:
			find_by['category'] = request.category
		subjects = Subject.find_by_arbitrary_field(**find_by)
		if subjects:
			for subject in subjects:
				result = main_pb2.Subject(
					uuid=subject.uuid,
					name=subject.name,
					category=subject.category[0].name,
					description=subject.description,
				)
				yield main_pb2.SubjectResponse(subject=result)
		result = main_pb2.Subject()
		return main_pb2.SubjectResponse(subject=result)

	def GetAllSubject(self, request, context):
		"""
		Gets a subject.
		gRPC calls this method when clients call the GetSubject rpc (method).
		Arguments:
			request (GetUserRequest): The incoming request.
			context: The gRPC connection context.
		Returns:
				subject (Subject): A subject.
		"""
		subjects = Subject.find_all()
		if subjects and len(subjects) > 0:
			for subject in subjects:
				result = main_pb2.Subject(
					uuid=subject.uuid,
					name=subject.name,
					category=subject.category[0].name,
					description=subject.description,
				)
				yield main_pb2.SubjectResponse(subject=result)
