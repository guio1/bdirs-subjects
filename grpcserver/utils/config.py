import os

from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())


class Config:
	DEBUG = False
	TESTING = False
	DRIVER = os.environ.get('DRIVER_NAME')
	DB_NAME = os.environ.get('DATABASE_NAME')
	SECRET_KEY = os.environ.get('SECRET_KEY')
	DB_USER = os.environ.get('DATABASE_USER')
	DB_HOST = os.environ.get('DATABASE_HOST')
	DB_PORT = os.environ.get('DATABASE_PORT')
	DATABASE_URI = os.environ.get('DATABASE_URI')
	DB_PASSWORD = os.environ.get('DATABASE_PASSWORD')


class Production(Config):
	DEBUG = False


class Development(Config):
	DEBUG = True
	DEVELOPMENT = True


class Testing(Config):
	DEBUG = True
	TESTING = True
	DB_NAME = os.environ.get('TEST_DATABASE_NAME')
	DB_USER = os.environ.get('TEST_DATABASE_USER')
	DB_HOST = os.environ.get('TEST_DATABASE_HOST')
	DB_PORT = os.environ.get('TEST_DATABASE_PORT')
	DATABASE_URI = os.environ.get('TEST_DATABASE_URI')
	DB_PASSWORD = os.environ.get('TEST_DATABASE_PASSWORD')
