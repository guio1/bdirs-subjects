# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: proto/main.proto

from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='proto/main.proto',
  package='grpcserver',
  syntax='proto3',
  serialized_options=None,
  serialized_pb=b'\n\x10proto/main.proto\x12\ngrpcserver\"l\n\x07Subject\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\x0c\n\x04uuid\x18\x02 \x01(\t\x12\r\n\x05level\x18\x03 \x01(\t\x12\x0f\n\x07\x63ountry\x18\x04 \x01(\t\x12\x13\n\x0b\x64\x65scription\x18\x05 \x01(\t\x12\x10\n\x08\x63\x61tegory\x18\x06 \x01(\t\";\n\x08\x43\x61tegory\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\x0c\n\x04uuid\x18\x02 \x01(\t\x12\x13\n\x0b\x64\x65scription\x18\x03 \x01(\t\"k\n\x14\x43reateSubjectRequest\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\x0f\n\x07\x63ountry\x18\x02 \x01(\t\x12\x10\n\x08\x63\x61tegory\x18\x03 \x01(\t\x12\r\n\x05level\x18\x04 \x01(\t\x12\x13\n\x0b\x64\x65scription\x18\x05 \x01(\t\"S\n\x11GetSubjectRequest\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\x0f\n\x07\x63ountry\x18\x02 \x01(\t\x12\x10\n\x08\x63\x61tegory\x18\x03 \x01(\t\x12\r\n\x05level\x18\x04 \x01(\t\")\n\x15GetAllSubjectsRequest\x12\x10\n\x08\x63\x61tegory\x18\x03 \x01(\t\"4\n\x0f\x43\x61tegoryRequest\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\x13\n\x0b\x64\x65scription\x18\x02 \x01(\t\"7\n\x0fSubjectResponse\x12$\n\x07subject\x18\x01 \x01(\x0b\x32\x13.grpcserver.Subject\":\n\x10\x43\x61tegoryResponse\x12&\n\x08\x63\x61tegory\x18\x01 \x01(\x0b\x32\x14.grpcserver.Category2\xd4\x02\n\x0eSubjectService\x12L\n\nGetSubject\x12\x1d.grpcserver.GetSubjectRequest\x1a\x1b.grpcserver.SubjectResponse\"\x00\x30\x01\x12P\n\rCreateSubject\x12 .grpcserver.CreateSubjectRequest\x1a\x1b.grpcserver.SubjectResponse\"\x00\x12M\n\x0e\x43reateCategory\x12\x1b.grpcserver.CategoryRequest\x1a\x1c.grpcserver.CategoryResponse\"\x00\x12S\n\rGetAllSubject\x12!.grpcserver.GetAllSubjectsRequest\x1a\x1b.grpcserver.SubjectResponse\"\x00\x30\x01\x62\x06proto3'
)




_SUBJECT = _descriptor.Descriptor(
  name='Subject',
  full_name='grpcserver.Subject',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='name', full_name='grpcserver.Subject.name', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='uuid', full_name='grpcserver.Subject.uuid', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='level', full_name='grpcserver.Subject.level', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='country', full_name='grpcserver.Subject.country', index=3,
      number=4, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='description', full_name='grpcserver.Subject.description', index=4,
      number=5, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='category', full_name='grpcserver.Subject.category', index=5,
      number=6, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=32,
  serialized_end=140,
)


_CATEGORY = _descriptor.Descriptor(
  name='Category',
  full_name='grpcserver.Category',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='name', full_name='grpcserver.Category.name', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='uuid', full_name='grpcserver.Category.uuid', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='description', full_name='grpcserver.Category.description', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=142,
  serialized_end=201,
)


_CREATESUBJECTREQUEST = _descriptor.Descriptor(
  name='CreateSubjectRequest',
  full_name='grpcserver.CreateSubjectRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='name', full_name='grpcserver.CreateSubjectRequest.name', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='country', full_name='grpcserver.CreateSubjectRequest.country', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='category', full_name='grpcserver.CreateSubjectRequest.category', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='level', full_name='grpcserver.CreateSubjectRequest.level', index=3,
      number=4, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='description', full_name='grpcserver.CreateSubjectRequest.description', index=4,
      number=5, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=203,
  serialized_end=310,
)


_GETSUBJECTREQUEST = _descriptor.Descriptor(
  name='GetSubjectRequest',
  full_name='grpcserver.GetSubjectRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='name', full_name='grpcserver.GetSubjectRequest.name', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='country', full_name='grpcserver.GetSubjectRequest.country', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='category', full_name='grpcserver.GetSubjectRequest.category', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='level', full_name='grpcserver.GetSubjectRequest.level', index=3,
      number=4, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=312,
  serialized_end=395,
)


_GETALLSUBJECTSREQUEST = _descriptor.Descriptor(
  name='GetAllSubjectsRequest',
  full_name='grpcserver.GetAllSubjectsRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='category', full_name='grpcserver.GetAllSubjectsRequest.category', index=0,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=397,
  serialized_end=438,
)


_CATEGORYREQUEST = _descriptor.Descriptor(
  name='CategoryRequest',
  full_name='grpcserver.CategoryRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='name', full_name='grpcserver.CategoryRequest.name', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='description', full_name='grpcserver.CategoryRequest.description', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=440,
  serialized_end=492,
)


_SUBJECTRESPONSE = _descriptor.Descriptor(
  name='SubjectResponse',
  full_name='grpcserver.SubjectResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='subject', full_name='grpcserver.SubjectResponse.subject', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=494,
  serialized_end=549,
)


_CATEGORYRESPONSE = _descriptor.Descriptor(
  name='CategoryResponse',
  full_name='grpcserver.CategoryResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='category', full_name='grpcserver.CategoryResponse.category', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=551,
  serialized_end=609,
)

_SUBJECTRESPONSE.fields_by_name['subject'].message_type = _SUBJECT
_CATEGORYRESPONSE.fields_by_name['category'].message_type = _CATEGORY
DESCRIPTOR.message_types_by_name['Subject'] = _SUBJECT
DESCRIPTOR.message_types_by_name['Category'] = _CATEGORY
DESCRIPTOR.message_types_by_name['CreateSubjectRequest'] = _CREATESUBJECTREQUEST
DESCRIPTOR.message_types_by_name['GetSubjectRequest'] = _GETSUBJECTREQUEST
DESCRIPTOR.message_types_by_name['GetAllSubjectsRequest'] = _GETALLSUBJECTSREQUEST
DESCRIPTOR.message_types_by_name['CategoryRequest'] = _CATEGORYREQUEST
DESCRIPTOR.message_types_by_name['SubjectResponse'] = _SUBJECTRESPONSE
DESCRIPTOR.message_types_by_name['CategoryResponse'] = _CATEGORYRESPONSE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

Subject = _reflection.GeneratedProtocolMessageType('Subject', (_message.Message,), {
  'DESCRIPTOR' : _SUBJECT,
  '__module__' : 'proto.main_pb2'
  # @@protoc_insertion_point(class_scope:grpcserver.Subject)
  })
_sym_db.RegisterMessage(Subject)

Category = _reflection.GeneratedProtocolMessageType('Category', (_message.Message,), {
  'DESCRIPTOR' : _CATEGORY,
  '__module__' : 'proto.main_pb2'
  # @@protoc_insertion_point(class_scope:grpcserver.Category)
  })
_sym_db.RegisterMessage(Category)

CreateSubjectRequest = _reflection.GeneratedProtocolMessageType('CreateSubjectRequest', (_message.Message,), {
  'DESCRIPTOR' : _CREATESUBJECTREQUEST,
  '__module__' : 'proto.main_pb2'
  # @@protoc_insertion_point(class_scope:grpcserver.CreateSubjectRequest)
  })
_sym_db.RegisterMessage(CreateSubjectRequest)

GetSubjectRequest = _reflection.GeneratedProtocolMessageType('GetSubjectRequest', (_message.Message,), {
  'DESCRIPTOR' : _GETSUBJECTREQUEST,
  '__module__' : 'proto.main_pb2'
  # @@protoc_insertion_point(class_scope:grpcserver.GetSubjectRequest)
  })
_sym_db.RegisterMessage(GetSubjectRequest)

GetAllSubjectsRequest = _reflection.GeneratedProtocolMessageType('GetAllSubjectsRequest', (_message.Message,), {
  'DESCRIPTOR' : _GETALLSUBJECTSREQUEST,
  '__module__' : 'proto.main_pb2'
  # @@protoc_insertion_point(class_scope:grpcserver.GetAllSubjectsRequest)
  })
_sym_db.RegisterMessage(GetAllSubjectsRequest)

CategoryRequest = _reflection.GeneratedProtocolMessageType('CategoryRequest', (_message.Message,), {
  'DESCRIPTOR' : _CATEGORYREQUEST,
  '__module__' : 'proto.main_pb2'
  # @@protoc_insertion_point(class_scope:grpcserver.CategoryRequest)
  })
_sym_db.RegisterMessage(CategoryRequest)

SubjectResponse = _reflection.GeneratedProtocolMessageType('SubjectResponse', (_message.Message,), {
  'DESCRIPTOR' : _SUBJECTRESPONSE,
  '__module__' : 'proto.main_pb2'
  # @@protoc_insertion_point(class_scope:grpcserver.SubjectResponse)
  })
_sym_db.RegisterMessage(SubjectResponse)

CategoryResponse = _reflection.GeneratedProtocolMessageType('CategoryResponse', (_message.Message,), {
  'DESCRIPTOR' : _CATEGORYRESPONSE,
  '__module__' : 'proto.main_pb2'
  # @@protoc_insertion_point(class_scope:grpcserver.CategoryResponse)
  })
_sym_db.RegisterMessage(CategoryResponse)



_SUBJECTSERVICE = _descriptor.ServiceDescriptor(
  name='SubjectService',
  full_name='grpcserver.SubjectService',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  serialized_start=612,
  serialized_end=952,
  methods=[
  _descriptor.MethodDescriptor(
    name='GetSubject',
    full_name='grpcserver.SubjectService.GetSubject',
    index=0,
    containing_service=None,
    input_type=_GETSUBJECTREQUEST,
    output_type=_SUBJECTRESPONSE,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='CreateSubject',
    full_name='grpcserver.SubjectService.CreateSubject',
    index=1,
    containing_service=None,
    input_type=_CREATESUBJECTREQUEST,
    output_type=_SUBJECTRESPONSE,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='CreateCategory',
    full_name='grpcserver.SubjectService.CreateCategory',
    index=2,
    containing_service=None,
    input_type=_CATEGORYREQUEST,
    output_type=_CATEGORYRESPONSE,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='GetAllSubject',
    full_name='grpcserver.SubjectService.GetAllSubject',
    index=3,
    containing_service=None,
    input_type=_GETALLSUBJECTSREQUEST,
    output_type=_SUBJECTRESPONSE,
    serialized_options=None,
  ),
])
_sym_db.RegisterServiceDescriptor(_SUBJECTSERVICE)

DESCRIPTOR.services_by_name['SubjectService'] = _SUBJECTSERVICE

# @@protoc_insertion_point(module_scope)
