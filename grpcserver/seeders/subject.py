from grpcserver.models.subject import Subject


def create_one(category_id):
	subject = Subject()
	new_subject = subject.create('Test subject', 'Optional description', category_id, 'Uganda', 'Primary')
	return new_subject
