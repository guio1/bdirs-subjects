from grpcserver.models.base import Base, Session, engine
from grpcserver.models.category import Category
from grpcserver.models.subject import Subject

session = Session()


# start/create all database tables
def start():
	# generate database schema
	Base.metadata.create_all(engine)


def drop_all_tables():
	Subject.__table__.drop(engine)
	Category.__table__.drop(engine)
