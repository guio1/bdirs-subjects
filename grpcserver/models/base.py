import os

from dotenv import load_dotenv, find_dotenv
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from grpcserver.utils.config import Development, Production, Testing

load_dotenv(find_dotenv())
config_class = Development
environment = ''
try:
	environment = os.environ.get("ENVIRONMENT") if os.environ.get("ENVIRONMENT") else os.environ["ENVIRONMENT"]
except Exception as e:
	environment = ''

if environment == "TESTING":
	config_class = Testing
elif environment == "PRODUCTION":
	config_class = Production

# Database value environment variables
user = config_class.DB_USER
host = config_class.DB_HOST
postgres = config_class.DRIVER
port = config_class.DB_PORT
password = config_class.DB_PASSWORD
database_name = config_class.DB_NAME
database_uri = config_class.DATABASE_URI

# Create database engine
if database_uri:
	engine = create_engine(database_uri)
else:
	engine = f"{postgres}://{user}:{password}@{host}:{port}/{database_name}"

Session = sessionmaker(bind=engine)

Base = declarative_base()
