import uuid
from sqlalchemy import Column, String, Integer
from sqlalchemy.orm import relationship
from sqlalchemy.orm.exc import NoResultFound

from grpcserver.models.base import Base, Session

session = Session()
session.expire_on_commit = False


class Category(Base):
	__tablename__ = "categories"

	id = Column('id', Integer, primary_key=True)
	name = Column(String(50), unique=True)
	uuid = Column(String(100), unique=True)
	description = Column(String(500))
	subject = relationship("Subject", backref="categories", passive_deletes=True)

	def __repr__(self):
		return f"<Category {self.name}, {self.description}>"

	def __str__(self):
		return f"{self.name} - {self.description}"

	@classmethod
	def find_by_name(cls, name):
		query = session.query(cls)
		try:
			cats = query.all()
			session.close()
			for cat in cats:
				if cat.name == name:
					return cat
		except NoResultFound:
			return None

	@classmethod
	def get_all(cls):
		query = session.query(cls)
		try:
			found_categories = query.all()
			session.close()
			return found_categories
		except NoResultFound:
			return None

	def create(self, name, description):
		self.name = name
		self.description = description
		self.uuid = uuid.uuid4().__str__()
		session.add(self)
		session.commit()
		session.close()
		return self
