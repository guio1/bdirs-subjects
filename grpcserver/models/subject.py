import uuid
from sqlalchemy import Column, String, Integer, ForeignKey
from sqlalchemy.orm.exc import NoResultFound

from grpcserver.models.base import Base, Session

session = Session()
session.expire_on_commit = False


class Subject(Base):
	__tablename__ = "subjects"

	id = Column('id', Integer, primary_key=True)
	name = Column(String(50), unique=True, nullable=False)
	uuid = Column(String(100), unique=True)
	level = Column(String(500), nullable=False)
	country = Column(String(500), nullable=False)
	description = Column(String(500))
	category_id = Column(Integer, ForeignKey("categories.id", ondelete="CASCADE"))
	# category = relationship(
	# 	"Category",
	# 	secondary=subjects_category_association,
	# 	backref=backref("children", cascade="all,delete")
	# )

	def __repr__(self):
		return f"<Subject {self.name}, of Category Id {self.category_id}, {self.description}>"

	def __str__(self):
		return f"{self.name} - {self.description}"

	def create(self, name, description, category, country, level):
		self.name = name
		self.level = level
		self.country = country
		self.uuid = uuid.uuid4().__str__()
		self.category_id = category
		self.description = description
		session.add(self)
		session.commit()
		session.close()

		return self

	@classmethod
	def find_by_name(cls, name):
		query = session.query(cls)
		try:
			found_subject = query.all()
			session.close()
			for subj in found_subject:
				if subj.name == name:
					return subj
		except NoResultFound:
			return None

	@classmethod
	def find_by_arbitrary_field(cls, **fields):
		try:
			query = session.query(cls)
			found_subjects = query.filter_by(**fields).all()
			return found_subjects
		except Exception as e:
			print(e, '====')
			return []

	@classmethod
	def find_all(cls) -> list:
		try:
			query = session.query(cls)
			found_subjects = query.all()
			return found_subjects
		except Exception as e:
			print(e, '====')
			return []
