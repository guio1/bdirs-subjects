import os
from concurrent import futures
import unittest
import grpc

from grpcserver.server.subject import SubjectService
from grpcserver.proto import main_pb2_grpc, main_pb2
from grpcserver.models.database import start as init_database, drop_all_tables
from grpcserver.seeders import subject as subject_seed
from grpcserver.seeders import category
from grpcserver.models.category import Category

os.environ['ENVIRONMENT'] = 'TESTING'


class TestSubjectRPC(unittest.TestCase):
	"""
	This Test class tests the Subjects RPC class,
	1. Get all subjects.
	2. Create subject.
	3. Get a subject.
	"""
	server_class = SubjectService
	max_workers = 10
	port = 6000

	def setUp(self):
		init_database()
		self.category = category.create_one()
		self.subject = subject_seed.create_one(self.category.id)
		self.server = grpc.server(futures.ThreadPoolExecutor(max_workers=self.max_workers))
		self.server.add_insecure_port(f'[::]:{self.port}')
		main_pb2_grpc.add_SubjectServiceServicer_to_server(self.server_class(), self.server)

	def tearDown(self):
		self.server.stop(0)
		drop_all_tables()

	# def test_server(self):
	# 	try:
	# 		with grpc.insecure_channel(f'localhost:{self.port}') as channel:
	# 			stub = main_pb2_grpc.SubjectServiceStub(channel)
	# 			response = stub.GetSubject(main_pb2.Subject(
	# 				name='Google',
	# 				level='primary',
	# 				country='Uganda',
	# 			))
	# 		print(response, '??????????')
	# 		# self.assertEqual(response.subject.name, 'Google')
	# 	except grpc.RpcError as e:
	# 		print(e, '!!!!!!!!!!!')

	def test_create_subject(self):
		cat = Category.get_all()
		print(":::::::", cat)
		try:
			with grpc.insecure_channel(f'localhost:{self.port}') as ch:
				stub = main_pb2_grpc.SubjectServiceStub(ch)
				response = stub.CreateSubject(main_pb2.CreateSubjectRequest(
					name='Test Subject',
					level='Primary',
					country='Uganda',
					category=self.category.name,
					description='The is also a test subject',
				))
			self.assertEqual(response.detail, 'google')
		except grpc.RpcError as e:
			print(e, '!!!!!!!!!!!')
