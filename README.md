<h3 align="center">
  <a href="https://github.com/bdirs/bdirs-web-api">
    BDIRS <span>(BDIRS BACHELOR DEGREE INFORMATION AND RECOMMENDATION SYSTEM)</span>
  </a>
</h3>

<p align="center">
  <a href="https://github.com/facebook/react-native/blob/master/LICENSE">
    <img src="https://img.shields.io/badge/license-MIT-blue.svg" alt="React Native is released under the MIT license." />
  </a>
  <a href="https://www.npmjs.org/package/react-native">
    <img src="https://badge.fury.io/js/react-native.svg" alt="Current npm package version." />
  </a>
  <a href="https://facebook.github.io/react-native/docs/contributing">
    <img src="https://img.shields.io/badge/PRs-welcome-brightgreen.svg" alt="PRs welcome!" />
  </a>
</p>

###### Background

This platform helps to address an issue with applying and getting the right degree program to to at
university more so public universities in Uganda.
Choosing the right program to do at university can help one get their dream job as well do something they love.

###### Micro service description.

The bdris subjects micro service handles all services and functionality that cater for subjects in BDIRS
This module or micro service is developed in [Python](https://www.python.org/) using the [Django Frame work](https://www.djangoproject.com/) and the [Django REST Framework](https://www.django-rest-framework.org/) as the tool kit for the API.

###### Project dependencies

- Python/ Django (DRF)

###### Project setup
- To run the project Locally, clone the repo
	1. cd into subjects
	2. create a virtual environment.
	3. activate the virtual environment.
	4. pip install the requirements.txt.
	5. to run the project use python3. run command is `python manage.py runserver`.

###### NOTE
Please find the full project report attached to repo. Some diagrams got mixed up but will be fixed soon

###### Contributors
- [Muhweezi Deo](https://github.com/MuhweziDeo)
- [Asiimwe Fahad](https://github.com/fahdjamy)
