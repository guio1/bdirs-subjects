# note: call scripts from /scripts

default:
	@echo "=============building Local API============="

########## for python grpc server.
grpcpythonserver:
	@echo "=== creating grpc server for python ====\n"
	sh scripts/build.sh

rundocker:
	docker container run  -p 6000:6060 subjects-python

killserver:
	pkill -f server.py
