import os
from concurrent import futures
import time
import grpc

from grpcserver.proto import main_pb2_grpc
from grpcserver.server import subject
from grpcserver.models.database import start as init_database

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


def server():
	port = os.environ.get('SERVER_PORT') if os.environ.get('SERVER_PORT') else 6000
	# Run a gRPC server with one thread.
	grpc_server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
	# Adds the service class to the server.
	main_pb2_grpc.add_SubjectServiceServicer_to_server(subject.SubjectService(), grpc_server)
	grpc_server.add_insecure_port('[::]:' + str(port))
	grpc_server.start()
	try:
		init_database()
	except AttributeError:
		pass
	print("Listening on port {}...".format(port))
	try:
		while True:
			time.sleep(_ONE_DAY_IN_SECONDS)
	except KeyboardInterrupt:
		grpc_server.stop(0)


if __name__ == "__main__":
	server()
