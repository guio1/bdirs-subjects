FROM python:3.8-slim-buster

RUN mkdir /app
RUN mkdir /app/grpcserver

COPY requirements.txt /app
RUN pip install -r /app/requirements.txt

COPY grpcserver /app/grpcserver
COPY server.py /app

ENV SERVER_PORT=6000

EXPOSE 6000
CMD ["python", "-u", "./app/server.py"]
